'use strict';

const
    fs = require('fs'),
    dateFormat = require('dateformat'),
    bookPageParser = require('./bookPageParser.js'),
    siteParser = require('./siteParser'),
    downloader = require('./download.js'),
    catalog = require('./catalog'),
    consoleLogFile = fs.createWriteStream('./log.txt', {encoding: 'utf8', autoClose: true}),
    maxQueueCount = 50;

console.log = function (message, callback) {
    const text = `${dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss.l')}: ${message} \n`;
    process.stdout.write(text);
    consoleLogFile.write(text, callback);
};

const onRelease = function () {
    catalog.saveCatalog();
    console.log(`Download Session end`, () => {
        consoleLogFile.end();
        process.exit();
    });
};

console.log(`Download Session started`);

siteParser.bookList((bookArray) => {

    bookArray = Array.from(new Set(bookArray));

    let downloadQueueCount = 0,
        downloadCount = 0,
        totalCount = bookArray.length,
        remainCount = totalCount,
        index = totalCount - 1;

    const finishQueueItem = function () {
        downloadQueueCount--;
        remainCount--;
        remainCount <= 0 ? onRelease() : schedule();
    };

    const downloadBook = function (error, book) {

        if (error || book === undefined) {
            finishQueueItem();
            return;
        }

        if (catalog.checkBook(book)) {
            console.log(`Book ${book.title} [${book.id}] already download, remain: ${remainCount - 1}`);
            finishQueueItem();
            return;
        }

        book.status = 2;
        catalog.saveBook(book);

        downloader.download(book, (error, book) => {

            if (error) {
                book.error = error;
                book.status = 0;
            } else {
                book.status = 1;
                console.log(`Total: ${totalCount}, download: ${++downloadCount}, remain: ${remainCount - 1}`);
            }

            catalog.saveBook(book);
            finishQueueItem();
        });
    };

    const schedule = function () {
        if (downloadQueueCount < maxQueueCount && index >= 0) {
            downloadQueueCount++;
            bookPageParser.getBook(bookArray[index--], downloadBook);
            schedule();
        }
    };

    schedule();
});
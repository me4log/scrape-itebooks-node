'use strict';

const
    http = require('http'),
    path = require('path'),
    fs = require('fs'),
    url = require('url'),
    wsOption = {
        flags: 'w',
        defaultEncoding: 'utf8',
        fd: null,
        mode: 0o666,
        autoClose: true
    };

const download = function (book, callback) {

    if (!book.url) {
        callback(new Error("Book not found"), book);
        return;
    }

    const downloadUrl = url.parse(book.url);

    if (!downloadUrl.hostname) {
        callback(new Error("Invalid URL"), book);
        return;
    }

    const downloadOptions = {
        hostname: downloadUrl.hostname,
        path: downloadUrl.path,
        method: 'GET',
        headers: {
            'Referer': book.referer
        }
    };

    const request = http.request(downloadOptions, response => {

        if (response.statusCode === 302) {
            book.url = response.headers.location;
            download(book, callback);
            return;
        }

        if (response.statusCode === 404) {
            callback(new Error('Status 404 Not found.'), book);
            return;
        }

        if (response.statusCode !== 200) {
            console.error()
            callback(new Error(`${response.statusCode} on download`), book);
            return;
        }

        console.log(`Start download ${book.id} (${book.title})`);

        const dir = path.join(__dirname, 'books', book.publisher);
        if (!fs.existsSync(dir)) fs.mkdirSync(dir);

        const file = path.join(dir, `${book.title} [${book.year}].pdf`);
        const ws = fs.createWriteStream(file, wsOption);
        ws.on('finish', () => {
            console.log(`End download ${book.id} (${book.title})`);
            callback(null, book);
        });

        response.pipe(ws);
    });

    request.end();
};

module.exports.download = download;
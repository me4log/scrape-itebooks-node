'use strict';

const
    fs = require('fs'),
    catalogFileName = './catalog.json';

let catalog;

const saveCatalog = function () {
    fs.writeFileSync(catalogFileName, JSON.stringify(catalog, null, '\t'), {encoding: 'utf8'});
};

const checkBook = function (book) {
    var b = catalog[book.isbn];
    return b !== undefined && b.status !== 0;
};

const saveBook = function (book) {
    catalog[book.isbn] = book;
};

const loadCatalog = function () {
    try {
        const catalogText = fs.readFileSync(catalogFileName, {encoding: 'utf8'});
        catalog = JSON.parse(catalogText);
    } catch (ex) {
        catalog = {};
        saveCatalog();
    }
};

loadCatalog();

module.exports.saveCatalog = saveCatalog;
module.exports.checkBook = checkBook;
module.exports.saveBook = saveBook;

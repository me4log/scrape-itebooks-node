'use strict';

const
    fs = require('fs'),
    jsdom = require('jsdom'),
    siteUrl = 'http://it-ebooks.info',
    url404 = `${siteUrl}/404/`,
    jquery = fs.readFileSync('./jquery.js', 'utf-8'),
    maxPublisherId = 16;

const parsePage = function (pageUrl, callback) {

    const doneFunction = function (error, window) {

        if (error || this.url === url404) {
            callback(url404);
            return;
        }

        const $ = window.$;
        const links = $('tr>td.top>table>tbody>tr>td[style]>a').map((index, elem) => {
            return $(elem).prop('href');
        }).toArray();
        callback(links);
    };

    const options = {
        url: pageUrl,
        src: [jquery],
        done: doneFunction
    };

    jsdom.env(options);
};

const bookList = function (callback) {

    let publisherId = 1;
    let page = 0;

    let result = [];

    const publisherUrl = () => {
        return `${siteUrl}/publisher/${publisherId}`;
    };

    const pageUrl = () => {
        return `${publisherUrl()}/page/${page}/`;
    };


    const parsePageCallback = function (parseResult) {

        if (parseResult === url404) {

            console.log(`Publisher: ${publisherId}, pages: ${page}`);
            if (++publisherId > maxPublisherId) {
                callback(result);
                return;
            }

            page = 0;
            parsePage(pageUrl(), parsePageCallback);
            return;
        }

        if (Array.isArray(parseResult))
            Array.prototype.push.apply(result, parseResult);

        page++;
        parsePage(pageUrl(), parsePageCallback);
    };

    parsePage(pageUrl(), parsePageCallback);
};

module.exports.bookList = bookList;
'use strict';

const
    fs = require('fs'),
    siteUrl = 'http://it-ebooks.info',
    url404 = `${siteUrl}/404/`,
    jquery = fs.readFileSync('./jquery.js', 'utf-8');

module.exports.getBook = function (bookUrl, callback) {

    const doneFunction = function (error, window) {

        if (error || this.url === url404) {
            callback(error || new Error("Url 404 Not Found"));
            return;
        }

        try {
            const $ = window.$;

            const book = {
                id: bookUrl,
                referer: this.url,
                publisher: $('table.ebook_view>tbody td table tr:contains("Publisher:")').children().last().text(),
                isbn: $('table.ebook_view>tbody td table tr:contains("ISBN:")').children().last().text(),
                year: $('table.ebook_view>tbody td table tr:contains("Year:")').children().last().text(),
                author: $('table.ebook_view>tbody td table tr:contains("By:")').children().last().children().last().text(),
                title: $('div[itemscope]>h1').text().replace('\\', '-').replace('/', '-').replace('?', '').replace(':', ''),
                url: $('table.ebook_view>tbody td table tr:contains("Download:")').children().last().find('a').attr('href')
            };

            book.url ? callback(null, book) : callback(new Error('Book page parse error. No book download URL found.'));

        } catch (ex) {
            ex.bookUrl = bookUrl;
            callback(ex);
        }

    };

    const options = {
        url: bookUrl,
        src: [jquery],
        done: doneFunction
    };

    require('jsdom').env(options);
};